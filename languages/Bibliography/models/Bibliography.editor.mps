<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:cf159657-cc01-486d-8291-8a5f19f2fb76(Bibliography.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="13" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="u5vo" ref="r:d4261686-76cd-4625-9a30-7257a3f0abb0(Bibliography.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1160590307797" name="usesFolding" index="S$F3r" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1198256887712" name="jetbrains.mps.lang.editor.structure.CellModel_Indent" flags="ng" index="3XFhqQ" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="3mEyjojMf7V">
    <ref role="1XX52x" to="u5vo:3mEyjojMf7K" resolve="TitleElement" />
    <node concept="3EZMnI" id="3mEyjojMf7X" role="2wV5jI">
      <node concept="3F0ifn" id="3mEyjojMf84" role="3EZMnx">
        <property role="3F0ifm" value="title" />
      </node>
      <node concept="3F0ifn" id="3mEyjojMf8a" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F0ifn" id="3mEyjojMf8i" role="3EZMnx">
        <property role="3F0ifm" value="{" />
      </node>
      <node concept="3F0A7n" id="3mEyjojMf8s" role="3EZMnx">
        <ref role="1NtTu8" to="u5vo:3mEyjojMf7L" resolve="value" />
      </node>
      <node concept="3F0ifn" id="3mEyjojMf8C" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
      <node concept="2iRfu4" id="3mEyjojMf80" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3mEyjojMlAr">
    <ref role="1XX52x" to="u5vo:3mEyjojMlAg" resolve="YearElement" />
    <node concept="3EZMnI" id="3mEyjojMlAt" role="2wV5jI">
      <node concept="3F0ifn" id="3mEyjojMlA$" role="3EZMnx">
        <property role="3F0ifm" value="year" />
      </node>
      <node concept="3F0ifn" id="3mEyjojMlAE" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F0A7n" id="3mEyjojMlAM" role="3EZMnx">
        <ref role="1NtTu8" to="u5vo:3mEyjojMlAh" resolve="value" />
      </node>
      <node concept="2iRfu4" id="3mEyjojMlAw" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3mEyjojMpsm">
    <ref role="1XX52x" to="u5vo:3mEyjojMpsb" resolve="JournalElement" />
    <node concept="3EZMnI" id="3mEyjojMpso" role="2wV5jI">
      <node concept="3F0ifn" id="3mEyjojMpsv" role="3EZMnx">
        <property role="3F0ifm" value="journal" />
      </node>
      <node concept="3F0ifn" id="3mEyjojMps_" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F0A7n" id="3mEyjojMpsH" role="3EZMnx">
        <ref role="1NtTu8" to="u5vo:3mEyjojMpsc" resolve="value" />
      </node>
      <node concept="2iRfu4" id="3mEyjojMpsr" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3mEyjojMtRP">
    <ref role="1XX52x" to="u5vo:3mEyjojMcxf" resolve="Bibliography" />
    <node concept="3EZMnI" id="3mEyjojMtS0" role="2wV5jI">
      <node concept="3EZMnI" id="3mEyjojMtSa" role="3EZMnx">
        <node concept="VPM3Z" id="3mEyjojMtSc" role="3F10Kt" />
        <node concept="3F0ifn" id="3mEyjojMtSe" role="3EZMnx">
          <property role="3F0ifm" value="Bibliography" />
        </node>
        <node concept="3F0ifn" id="3mEyjojMtSn" role="3EZMnx">
          <property role="3F0ifm" value=":" />
        </node>
        <node concept="3F0A7n" id="3mEyjojMtSv" role="3EZMnx">
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
        <node concept="2iRfu4" id="3mEyjojMtSf" role="2iSdaV" />
        <node concept="3F0ifn" id="3mEyjojMtT1" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
      </node>
      <node concept="3EZMnI" id="3mEyjojMzcq" role="3EZMnx">
        <node concept="2iRfu4" id="3mEyjojMzcr" role="2iSdaV" />
        <node concept="3XFhqQ" id="3mEyjojMzcI" role="3EZMnx" />
        <node concept="3XFhqQ" id="3mEyjojMzcS" role="3EZMnx" />
        <node concept="3F2HdR" id="3mEyjojMtU_" role="3EZMnx">
          <property role="S$F3r" value="true" />
          <ref role="1NtTu8" to="u5vo:3mEyjojMcxn" resolve="biblioElements" />
          <node concept="2iRkQZ" id="3mEyjojMtUB" role="2czzBx" />
        </node>
      </node>
      <node concept="3F0ifn" id="3mEyjojMtS$" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
      <node concept="2iRkQZ" id="3mEyjojMtS3" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3mEyjojMCkn">
    <ref role="1XX52x" to="u5vo:3mEyjojMcxe" resolve="BibliographyRepository" />
    <node concept="3EZMnI" id="3mEyjojMXbU" role="2wV5jI">
      <node concept="3F0A7n" id="76ad064Sp0u" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="2iRkQZ" id="3mEyjojMXbV" role="2iSdaV" />
      <node concept="3EZMnI" id="3mEyjojMXbY" role="3EZMnx">
        <node concept="l2Vlx" id="3mEyjojMXbZ" role="2iSdaV" />
        <node concept="VPM3Z" id="3mEyjojMXc0" role="3F10Kt" />
        <node concept="3XFhqQ" id="3mEyjojMXc6" role="3EZMnx" />
        <node concept="3XFhqQ" id="3mEyjojMXcZ" role="3EZMnx" />
        <node concept="3F2HdR" id="3mEyjojMXd7" role="3EZMnx">
          <ref role="1NtTu8" to="u5vo:3mEyjojMcxi" resolve="bibliographies" />
          <node concept="2iRkQZ" id="3mEyjojMXda" role="2czzBx" />
          <node concept="VPM3Z" id="3mEyjojMXdb" role="3F10Kt" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3mEyjojNxZd">
    <ref role="1XX52x" to="u5vo:3mEyjojMcxp" resolve="AuthorElement" />
    <node concept="3EZMnI" id="3mEyjojNBs1" role="2wV5jI">
      <node concept="3EZMnI" id="3mEyjojNBsb" role="3EZMnx">
        <node concept="VPM3Z" id="3mEyjojNBsd" role="3F10Kt" />
        <node concept="3F0ifn" id="3mEyjojNBsf" role="3EZMnx">
          <property role="3F0ifm" value="authors" />
        </node>
        <node concept="3F0ifn" id="3mEyjojNBsp" role="3EZMnx">
          <property role="3F0ifm" value="=" />
        </node>
        <node concept="3F0ifn" id="3mEyjojNBsB" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
        <node concept="l2Vlx" id="3mEyjojNBsg" role="2iSdaV" />
      </node>
      <node concept="3XFhqQ" id="1MJpaJhjaa_" role="3EZMnx" />
      <node concept="3F2HdR" id="76ad064Pv40" role="3EZMnx">
        <ref role="1NtTu8" to="u5vo:76ad064Pe2C" resolve="authors" />
        <node concept="2iRkQZ" id="76ad064Pv42" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="3mEyjojNBsG" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
      <node concept="2iRkQZ" id="3mEyjojNBs4" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3mEyjojO2Am">
    <ref role="1XX52x" to="u5vo:3mEyjojO2A1" resolve="Person" />
    <node concept="3EZMnI" id="3mEyjojO2Ao" role="2wV5jI">
      <node concept="3F0A7n" id="76ad064RSKY" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0A7n" id="3mEyjojO2Ay" role="3EZMnx">
        <ref role="1NtTu8" to="u5vo:3mEyjojO2A5" resolve="firstname" />
      </node>
      <node concept="3F0ifn" id="3mEyjojO2AC" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F0A7n" id="3mEyjojO2AK" role="3EZMnx">
        <ref role="1NtTu8" to="u5vo:3mEyjojO2A7" resolve="middlename" />
      </node>
      <node concept="3F0ifn" id="3mEyjojO2AU" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F0A7n" id="3mEyjojO2Bd" role="3EZMnx">
        <ref role="1NtTu8" to="u5vo:3mEyjojO2Aa" resolve="lastname" />
      </node>
      <node concept="2iRfu4" id="3mEyjojO2Ar" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3mEyjojO82y">
    <ref role="1XX52x" to="u5vo:3mEyjojO82n" resolve="AuthorRef" />
    <node concept="3EZMnI" id="3mEyjojO82$" role="2wV5jI">
      <node concept="3F0ifn" id="3mEyjojO832" role="3EZMnx">
        <property role="3F0ifm" value="Person" />
      </node>
      <node concept="1iCGBv" id="1MJpaJhiayh" role="3EZMnx">
        <ref role="1NtTu8" to="u5vo:1MJpaJhhizN" resolve="person" />
        <node concept="1sVBvm" id="1MJpaJhiayj" role="1sWHZn">
          <node concept="3F0A7n" id="3HHFWlbgHNP" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="u5vo:3mEyjojO2A5" resolve="firstname" />
          </node>
        </node>
      </node>
      <node concept="l2Vlx" id="3mEyjojO82B" role="2iSdaV" />
    </node>
  </node>
</model>

