<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:d4261686-76cd-4625-9a30-7257a3f0abb0(Bibliography.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="9" />
    <use id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access" version="0" />
    <use id="df345b11-b8c7-4213-ac66-48d2a9b75d88" name="jetbrains.mps.baseLanguageInternal" version="0" />
    <use id="0ae47ad3-5abd-486c-ac0f-298884f39393" name="jetbrains.mps.baseLanguage.constructors" version="0" />
    <use id="c0080a47-7e37-4558-bee9-9ae18e690549" name="jetbrains.mps.lang.extension" version="2" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="3mEyjojMcxe">
    <property role="EcuMT" value="3867054095404157006" />
    <property role="TrG5h" value="BibliographyRepository" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="3mEyjojMcxi" role="1TKVEi">
      <property role="IQ2ns" value="3867054095404157010" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="bibliographies" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="3mEyjojMcxf" resolve="Bibliography" />
    </node>
    <node concept="PrWs8" id="76ad064REUF" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="3mEyjojMcxf">
    <property role="EcuMT" value="3867054095404157007" />
    <property role="TrG5h" value="Bibliography" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="1MJpaJhilEv" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="3mEyjojMcxn" role="1TKVEi">
      <property role="IQ2ns" value="3867054095404157015" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="biblioElements" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="3mEyjojMcxk" resolve="BiblioElement" />
    </node>
  </node>
  <node concept="1TIwiD" id="3mEyjojMcxk">
    <property role="EcuMT" value="3867054095404157012" />
    <property role="TrG5h" value="BiblioElement" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="3mEyjojMcxp">
    <property role="EcuMT" value="3867054095404157017" />
    <property role="TrG5h" value="AuthorElement" />
    <ref role="1TJDcQ" node="3mEyjojMcxk" resolve="BiblioElement" />
    <node concept="1TJgyj" id="76ad064Pe2C" role="1TKVEi">
      <property role="IQ2ns" value="8181408854200082600" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="authors" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="3mEyjojNcUt" resolve="Author" />
    </node>
    <node concept="PrWs8" id="87Zig6wer2" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="3mEyjojMf7K">
    <property role="EcuMT" value="3867054095404167664" />
    <property role="TrG5h" value="TitleElement" />
    <ref role="1TJDcQ" node="3mEyjojMcxk" resolve="BiblioElement" />
    <node concept="1TJgyi" id="3mEyjojMf7L" role="1TKVEl">
      <property role="IQ2nx" value="3867054095404167665" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="3mEyjojMlAg">
    <property role="EcuMT" value="3867054095404194192" />
    <property role="TrG5h" value="YearElement" />
    <ref role="1TJDcQ" node="3mEyjojMcxk" resolve="BiblioElement" />
    <node concept="1TJgyi" id="3mEyjojMlAh" role="1TKVEl">
      <property role="IQ2nx" value="3867054095404194193" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="3mEyjojMpsb">
    <property role="EcuMT" value="3867054095404209931" />
    <property role="TrG5h" value="JournalElement" />
    <ref role="1TJDcQ" node="3mEyjojMcxk" resolve="BiblioElement" />
    <node concept="1TJgyi" id="3mEyjojMpsc" role="1TKVEl">
      <property role="IQ2nx" value="3867054095404209932" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="3mEyjojNcUt">
    <property role="EcuMT" value="3867054095404420765" />
    <property role="TrG5h" value="Author" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="3mEyjojO2A1">
    <property role="EcuMT" value="3867054095404640641" />
    <property role="TrG5h" value="Person" />
    <ref role="1TJDcQ" node="3mEyjojNcUt" resolve="Author" />
    <node concept="1TJgyi" id="3mEyjojO2A5" role="1TKVEl">
      <property role="IQ2nx" value="3867054095404640645" />
      <property role="TrG5h" value="firstname" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3mEyjojO2A7" role="1TKVEl">
      <property role="IQ2nx" value="3867054095404640647" />
      <property role="TrG5h" value="middlename" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3mEyjojO2Aa" role="1TKVEl">
      <property role="IQ2nx" value="3867054095404640650" />
      <property role="TrG5h" value="lastname" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="PrWs8" id="76ad064Rt4X" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="3mEyjojO82n">
    <property role="EcuMT" value="3867054095404662935" />
    <property role="TrG5h" value="AuthorRef" />
    <ref role="1TJDcQ" node="3mEyjojNcUt" resolve="Author" />
    <node concept="1TJgyj" id="1MJpaJhhizN" role="1TKVEi">
      <property role="IQ2ns" value="2066981443099502835" />
      <property role="20kJfa" value="person" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="3mEyjojO2A1" resolve="Person" />
    </node>
  </node>
</model>

