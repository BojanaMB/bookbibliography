<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6935b0cb-a461-4d6a-b1ed-62b36505bee6(Bibliography.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="82d36913-1940-4be3-bd33-efbc4c3fcff0" name="Bibliography" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="82d36913-1940-4be3-bd33-efbc4c3fcff0" name="Bibliography">
      <concept id="3867054095404157017" name="Bibliography.structure.AuthorElement" flags="ng" index="Ao$5J">
        <child id="8181408854200082600" name="authors" index="3PnHiD" />
      </concept>
      <concept id="3867054095404157006" name="Bibliography.structure.BibliographyRepository" flags="ng" index="Ao$5S">
        <child id="3867054095404157010" name="bibliographies" index="Ao$5$" />
      </concept>
      <concept id="3867054095404157007" name="Bibliography.structure.Bibliography" flags="ng" index="Ao$5T">
        <child id="3867054095404157015" name="biblioElements" index="Ao$5x" />
      </concept>
      <concept id="3867054095404167664" name="Bibliography.structure.TitleElement" flags="ng" index="AoBz6">
        <property id="3867054095404167665" name="value" index="AoBz7" />
      </concept>
      <concept id="3867054095404640641" name="Bibliography.structure.Person" flags="ng" index="AuE2R">
        <property id="3867054095404640647" name="middlename" index="AuE2L" />
        <property id="3867054095404640645" name="firstname" index="AuE2N" />
        <property id="3867054095404640650" name="lastname" index="AuE2W" />
      </concept>
    </language>
  </registry>
  <node concept="Ao$5S" id="3mEyjojNsHs">
    <property role="TrG5h" value="Bibliografska lista" />
    <node concept="Ao$5T" id="iZOLTKRRXW" role="Ao$5$">
      <property role="TrG5h" value="bibliografija1" />
      <node concept="Ao$5J" id="iZOLTKRRY0" role="Ao$5x">
        <node concept="AuE2R" id="iZOLTKRRY3" role="3PnHiD">
          <property role="TrG5h" value="ime1" />
          <property role="AuE2N" value="bojana" />
          <property role="AuE2L" value="bojana" />
          <property role="AuE2W" value="b" />
        </node>
        <node concept="AuE2R" id="iZOLTKRRYk" role="3PnHiD">
          <property role="TrG5h" value="ime2" />
          <property role="AuE2N" value="milena" />
          <property role="AuE2L" value="milena" />
          <property role="AuE2W" value="m" />
        </node>
      </node>
      <node concept="AoBz6" id="iZOLTKRRYt" role="Ao$5x">
        <property role="AoBz7" value="title1" />
      </node>
    </node>
    <node concept="Ao$5T" id="iZOLTKRRYH" role="Ao$5$">
      <property role="TrG5h" value="bibliografija2" />
      <node concept="Ao$5J" id="iZOLTKRRYO" role="Ao$5x" />
    </node>
  </node>
</model>

