package Bibliography.constraints;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.base.BaseConstraintsDescriptor;
import java.util.Map;
import org.jetbrains.mps.openapi.language.SReferenceLink;
import jetbrains.mps.smodel.runtime.ReferenceConstraintsDescriptor;
import jetbrains.mps.smodel.runtime.base.BaseReferenceConstraintsDescriptor;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.smodel.runtime.ReferenceScopeProvider;
import jetbrains.mps.smodel.runtime.base.BaseScopeProvider;
import org.jetbrains.mps.openapi.model.SNodeReference;
import jetbrains.mps.scope.Scope;
import jetbrains.mps.smodel.runtime.ReferenceConstraintsContext;
import jetbrains.mps.scope.EmptyScope;
import java.util.HashMap;
import jetbrains.mps.smodel.SNodePointer;
import org.jetbrains.mps.openapi.language.SConcept;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;

public class AuthorRef_Constraints extends BaseConstraintsDescriptor {
  public AuthorRef_Constraints() {
    super(CONCEPTS.AuthorRef$z3);
  }

  @Override
  protected Map<SReferenceLink, ReferenceConstraintsDescriptor> getSpecifiedReferences() {
    BaseReferenceConstraintsDescriptor d0 = new BaseReferenceConstraintsDescriptor(LINKS.person$9w4b, this) {
      @Override
      public boolean hasOwnScopeProvider() {
        return true;
      }
      @Nullable
      @Override
      public ReferenceScopeProvider getScopeProvider() {
        return new BaseScopeProvider() {
          @Override
          public SNodeReference getSearchScopeValidatorNode() {
            return breakingNode_bppva7_a0a0a0a0a1a0a0a0c;
          }
          @Override
          public Scope createScope(final ReferenceConstraintsContext _context) {
            Scope scope = Scope.getScope(_context.getContextNode(), _context.getContainmentLink(), _context.getPosition(), CONCEPTS.Person$UF);
            return (scope == null ? new EmptyScope() : scope);
          }
        };
      }
    };
    Map<SReferenceLink, ReferenceConstraintsDescriptor> references = new HashMap<SReferenceLink, ReferenceConstraintsDescriptor>();
    references.put(d0.getReference(), d0);
    return references;
  }
  private static final SNodePointer breakingNode_bppva7_a0a0a0a0a1a0a0a0c = new SNodePointer("r:a2a7e978-bf89-4681-b70e-c3b54a2ecc58(Bibliography.constraints)", "146363844082616833");

  private static final class CONCEPTS {
    /*package*/ static final SConcept AuthorRef$z3 = MetaAdapterFactory.getConcept(0x82d3691319404be3L, 0xbd33efbc4c3fcff0L, 0x35aa893613d08097L, "Bibliography.structure.AuthorRef");
    /*package*/ static final SConcept Person$UF = MetaAdapterFactory.getConcept(0x82d3691319404be3L, 0xbd33efbc4c3fcff0L, 0x35aa893613d02981L, "Bibliography.structure.Person");
  }

  private static final class LINKS {
    /*package*/ static final SReferenceLink person$9w4b = MetaAdapterFactory.getReferenceLink(0x82d3691319404be3L, 0xbd33efbc4c3fcff0L, 0x35aa893613d08097L, 0x1caf64abd14528f3L, "person");
  }
}
